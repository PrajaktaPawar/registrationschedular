package registrationScheduler.util;

import java.util.Scanner;

/**
 * FileProcessor class to read file and pass single line at a time
 * 
 * @author Prajakta Pawar
 */
public class FileProcessor {

	private Scanner scanner = null;
	private String status = null;

	public FileProcessor(Scanner scannerIn, String statusIn) {
		scanner = scannerIn;
		status = statusIn;
		Logger.writeMessage("In FileProcessor constructor,scanner object assigned..", Logger.DebugLevel.CONSTRUCTOR);

	}

	/*
	 * class FileEmptyException extends Exception {
	 *//**
		 * user defined exception
		 *//*
		 * private static final long serialVersionUID = 1L;
		 * 
		 * public FileEmptyException(String msg) { super(msg); } }
		 */
	public synchronized void closeScanner() {
		scanner.close();
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * synchronized readString Method return a single line at a time from file
	 * @return 	single line from file
	 */
	public synchronized String readString() {

		String line = null;
		try {

			if (!scanner.hasNextLine()) {
				return null;
				// throw new FileEmptyException("Registration preference input
				// file is empty..");
			}
			line = scanner.nextLine();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		} finally {

		}

		return line;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Status: " + status;
	}
}
