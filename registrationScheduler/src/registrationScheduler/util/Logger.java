
package registrationScheduler.util;

public class Logger {

	public static enum DebugLevel {
		CONSTRUCTOR,
		THREAD_RUN,
    	RESULT_DATA_STRUCTURE,
    	STORE_DATA_STRUCTURE,
    	NO_OUTPUT
	};

	private static DebugLevel debugLevel;

	public static void setDebugValue(int levelIn) {
		switch (levelIn) {
		  case 4: debugLevel = DebugLevel.CONSTRUCTOR; break;
		  case 3: debugLevel = DebugLevel.THREAD_RUN; break;
		  case 2: debugLevel = DebugLevel.RESULT_DATA_STRUCTURE; break;
		  case 1: debugLevel = DebugLevel.STORE_DATA_STRUCTURE; break;
		  case 0: debugLevel = DebugLevel.NO_OUTPUT; break;
		}
	}

	public static void setDebugValue(DebugLevel levelIn) {
		debugLevel = levelIn;
	}

	// @return None
	public static void writeMessage(String message, DebugLevel levelIn) {
		if (levelIn == debugLevel)
			System.out.println(message);
	}

	public String toString() {
		return "Debug Level is " + debugLevel;
	}
}
