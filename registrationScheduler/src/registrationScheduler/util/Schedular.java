package registrationScheduler.util;

import java.util.Map;

import registrationScheduler.objectPool.CoursesInfo;
import registrationScheduler.objectPool.ObjectPool;
import registrationScheduler.store.Student;
import registrationScheduler.util.Logger;

/**
 * Schedular 	class which runs actual algorithm for course assignment
 * @author Prajakta Pawar
 *
 */
public class Schedular {

	private ObjectPool objectpoolInstance = null; 

	public Schedular(){
		objectpoolInstance = ObjectPool.getInstance();
		objectpoolInstance.addObjects();
		Logger.writeMessage("In Schedular constructor", Logger.DebugLevel.CONSTRUCTOR);
	}

	/**
	 * 
	 * @param student      Method to process student,assign courses to student if seats are available
	 */
	public synchronized void processStudent(Student student) {
		for(Map.Entry<Character, Integer> entry : student.getPreferencesList().entrySet()){
			CoursesInfo courseI = null;
			CoursesInfo randomCourse = null;
			try {
				
				courseI = objectpoolInstance.borrowObject(entry.getKey(),0);
				while(courseI == null){
					courseI = objectpoolInstance.borrowObject(entry.getKey(),0);
				}
				if(courseI.allocateIfAvailable()){
					student.allocateCourse(entry.getKey());	
					Logger.writeMessage("Allocated student "+ student.getStudentID()+" to Course "+ courseI 
							+"\n"
							+"----------------------------------------------\n"
							, Logger.DebugLevel.RESULT_DATA_STRUCTURE);
				}
				else{
					int next = 0;
					randomCourse = objectpoolInstance.borrowObject(null,next);

					while(student.getPreferencesList().containsKey(randomCourse.getCourseID()) ||
							student.getAllocatedCourses().containsKey(randomCourse.getCourseID()) )
					{
						next++;
						objectpoolInstance.returnObject(randomCourse);
						randomCourse = objectpoolInstance.borrowObject(null,next);
					}

					if(randomCourse.allocateIfAvailable()){
						student.allocateCourse(randomCourse.getCourseID());
						Logger.writeMessage("Allocated random course to "+ student.getStudentID()+" course id: "+ randomCourse 
								+"\n"
								+"----------------------------------------------\n"
								, Logger.DebugLevel.RESULT_DATA_STRUCTURE);
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println(student.getStudentID());
			}finally {
				objectpoolInstance.returnObject(randomCourse);
				objectpoolInstance.returnObject(courseI);
			}
		}
	}
	/**
	 * Method to update courses allocated to student
	 * @param st student object
	 * @param tabSeparatedArray list of courses(add drop list) 
	 */
	public void updateStudent(Student st ,String[] tabSeparatedArray) {
		// TODO Auto-generated method stub
		if(tabSeparatedArray[1].charAt(0) == '0'){
			for(int i=2;i<tabSeparatedArray.length;i++){
				dropCourse(st,tabSeparatedArray[i].charAt(0));
			}
		}else{
			for(int i=2;i<tabSeparatedArray.length;i++){
				addCourse(st,tabSeparatedArray[i].charAt(0));
			}
		}
	}
	/**
	 * Method to process add courses requirement of student
	 * @param student student object
	 * @param courseID course id
	 */
	private void addCourse(Student student,Character courseID) {
		// TODO Auto-generated method stub
		CoursesInfo courseI = null;
		
		courseI = objectpoolInstance.borrowObject(courseID,0);
		while(courseI == null){
			courseI = objectpoolInstance.borrowObject(courseID,0);
		}
		try {
			if(courseI.allocateIfAvailable()){
				student.allocateCourse(courseID);
				Logger.writeMessage("added "+ courseI +" for student "+ student.getStudentID(), Logger.DebugLevel.RESULT_DATA_STRUCTURE);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			objectpoolInstance.returnObject(courseI);
		}
		
	}
	/**
	 * Method to process drop courses request of student
	 * @param student student object
	 * @param courseID course id
	 */
	private void dropCourse(Student student,Character courseID) {
		// TODO Auto-generated method stub
		CoursesInfo courseI =null;
		
		courseI = objectpoolInstance.borrowObject(courseID,0);
		while(courseI == null){
			courseI = objectpoolInstance.borrowObject(courseID,0);
		}

		try {
			if(courseI.dropIfNecessary()){
				student.deAllocateCourse(courseID);
				Logger.writeMessage("dropped "+ courseI +" to AssignedCourses for student "+ student.getStudentID(), Logger.DebugLevel.RESULT_DATA_STRUCTURE);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			objectpoolInstance.returnObject(courseI);
		}
	}

	
}
