package registrationScheduler.util;

/**
 * 
 * @author Prajakta Pawar
 * interface to write schedules to file.
 */
public interface FileDisplayInterface {
	void writeSchedulesToFile(String outputFileName);
}
