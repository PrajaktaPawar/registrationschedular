package registrationScheduler.util;

/**
 * 
 * @author Prajakta Pawar
 * interface to write schedules to screen.
 */
public interface StdoutDisplayInterface {
	public void writeSchedulesToScreen();
}
