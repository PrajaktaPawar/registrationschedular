package registrationScheduler.store;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import registrationScheduler.util.Logger;

/**
 * Student details
 * @author Prajakta Pawar
 *
 */
public class Student {

	
	private String studentID;

	public String getStudentID() {
		return studentID;
	}

	public void setStudentID(String studentID) {
		this.studentID = studentID;
	}

	/**
	 * Map to hold Preference list of courses given by student
	 */
	private Map<Character,Integer>preferencesList = null;
	
	/**
	 * Map to hold courses allocated to student
	 */
	private Map<Character,Integer>allocatedCourses = new HashMap<Character,Integer>();
	private int preferenceScore = 0;
	
	public Map<Character, Integer> getPreferencesList() {
		return preferencesList;
	}

	public void setPreferencesList(Map<Character, Integer> preferencesList) {
		this.preferencesList = preferencesList;
	}

	public Map<Character, Integer> getAllocatedCourses() {
		return allocatedCourses;
	}

	public int getPreferenceScore() {
		return preferenceScore;
	}

	/**
	 * Student constructor
	 * @param sID - student id
	 * @param preferencesListIN - preferences list
	 */
	public Student(String sID,Map<Character,Integer>preferencesListIN){
		studentID = sID;
		preferencesList = preferencesListIN;
		Logger.writeMessage("In Student constructor,student id and preference list initialized..\n"
				+ this.toString()
				, Logger.DebugLevel.CONSTRUCTOR);
	}

	/**
	 * Method to allocate course to student if seats are available 
	 * and change preference score accordingly.
	 * @param key - course id
	 */
	public void allocateCourse(Character key) {
		// TODO Auto-generated method stub
		if(allocatedCourses.containsKey(key))
			return;
		
		Integer prefScore = preferencesList.get(key);
		if(prefScore != null){
			allocatedCourses.put(key, prefScore);
			preferenceScore += prefScore;
		}else{
			allocatedCourses.put(key, 1);
			preferenceScore += 1;
		}
	}

	/**
	 * Method to remove course from allocated list and 
	 * change preference score accordingly.
	 * @param courseID - course id
	 */
	public void deAllocateCourse(Character courseID) {
		// TODO Auto-generated method stub
		Integer prefScore = preferencesList.get(courseID);
		if(prefScore == null){
			prefScore = 1;
		}		
		if(null != allocatedCourses.remove(courseID))
			preferenceScore -= prefScore;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return " Student Id: "+studentID +
				" \n Choices :"+ Arrays.toString(preferencesList.keySet().toArray()) + 
				" \n Preference score : "+ preferenceScore + 
				" \n Assigned Courses :"+ Arrays.toString(allocatedCourses.keySet().toArray());
			}
	}
