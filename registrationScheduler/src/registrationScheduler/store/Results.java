package registrationScheduler.store;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import registrationScheduler.util.FileDisplayInterface;
import registrationScheduler.util.Logger;
import registrationScheduler.util.StdoutDisplayInterface;

/**
 * Class to write results to file and screen.
 * 
 * @author Prajakta Pawar
 *
 */
public class Results implements StdoutDisplayInterface, FileDisplayInterface {
	/**
	 * Map to hold student data
	 */
	private Map<String, Student> studentInfoMap = new HashMap<String, Student>();

	public synchronized Map<String, Student> getStudentInfoMap() {
		return studentInfoMap;
	}

	/**
	 * @param sID
	 *            - student id
	 * @return student object from student id
	 */
	public synchronized Student getStudent(String sID) {
		Student s = studentInfoMap.get(sID);
		return s;
	}

	/**
	 * @param line - student record read from file.
	 * @return student object
	 */
	public synchronized Student addStudentDataToMap(String line) {
		String[] tabSeparatedArray = line.split("\\s");

		Map<Character, Integer> prefList = new HashMap<Character, Integer>();
		for (int i = 1; i < tabSeparatedArray.length; i++) {
			prefList.put(tabSeparatedArray[i].charAt(0), 7 - i);
		}
		Student st = new Student(tabSeparatedArray[0], prefList);

		studentInfoMap.put(st.getStudentID(), st);
		Logger.writeMessage(
				"Student added to Student Map\n"
				+ st.getStudentID()
				+ st.getPreferencesList()
				+ "\n"
				+ "--------------------------------------------------------------"
				, Logger.DebugLevel.RESULT_DATA_STRUCTURE);		
		return st;
	}

	/**
	 * Method to write results to output file.
	 */
	@Override
	public void writeSchedulesToFile(String outputFileName) {
		// TODO Auto-generated method stub
		BufferedWriter bufferedWriter = null;
		FileWriter fileWriter = null;
		double finalScore = 0;
		double avgScore = 0;
		try {
			fileWriter = new FileWriter(outputFileName);
			bufferedWriter = new BufferedWriter(fileWriter);
			for (Map.Entry<String, Student> entry : studentInfoMap.entrySet()) {
				bufferedWriter.write(entry.getValue().getStudentID() + " " );
				Map<Character,Integer> segCourses = entry.getValue().getAllocatedCourses();
				for (Map.Entry<Character,Integer> segCourse : segCourses.entrySet()){
					bufferedWriter.write(segCourse.getKey() + " ");
				}
				bufferedWriter.write(entry.getValue().getPreferenceScore() + "\n");
				finalScore += entry.getValue().getPreferenceScore();
			}
			bufferedWriter.write("\nFinal Score is: " + finalScore + "\n");
			avgScore = finalScore / 80;
			bufferedWriter.write("\nAverage Preference Score is: " + avgScore);
		} catch (FileNotFoundException ex) {
			System.err.println("Unable to open file.. " + outputFileName + " Not found");
			ex.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {

				if (bufferedWriter != null)
					bufferedWriter.close();

				if (fileWriter != null)
					fileWriter.close();

			} catch (IOException ex) {

				ex.printStackTrace();

			}
		}
	}
	/**
	 * Method to write results to screens.
	 */
	@Override
	public void writeSchedulesToScreen() {
		// TODO Auto-generated method stub
		double finalScore = 0;
		double avgScore = 0;
		StringBuilder sb = new StringBuilder();
		for (Map.Entry<String, Student> entry : studentInfoMap.entrySet()) {
			
			sb.append(entry.getValue().getStudentID()+" ");
			
			Map<Character,Integer> segCourses = entry.getValue().getAllocatedCourses();
			for (Map.Entry<Character,Integer> segCourse : segCourses.entrySet()){
				 sb.append(segCourse.getKey() + " ");
			}
			
			sb.append(entry.getValue().getPreferenceScore()+"\n");
			finalScore += entry.getValue().getPreferenceScore();
		}
		Logger.writeMessage(
				"Contents of Student Map: \n"
				+ sb.toString()
				+ "\n"
				+ "--------------------------------------------------------------"
				, Logger.DebugLevel.STORE_DATA_STRUCTURE);
		avgScore = finalScore / 80;
		
		Logger.writeMessage(
				"final score is::"+ finalScore
				+"\nAverage Preference Score is::" + avgScore
				+ "\n"
				+ "--------------------------------------------------------------"
				, Logger.DebugLevel.STORE_DATA_STRUCTURE);	
		Logger.writeMessage(
				"final score is::"+ finalScore
				+"\nAverage Preference Score is::" + avgScore
				+ "\n"
				+ "--------------------------------------------------------------"
				, Logger.DebugLevel.NO_OUTPUT);	
	}
	
	@Override
	public String toString() {
		return Arrays.toString(studentInfoMap.keySet().toArray());
	};

}
