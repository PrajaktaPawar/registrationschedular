package registrationScheduler.objectPool;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import registrationScheduler.objectPool.CoursesInfo;
import registrationScheduler.util.Logger;

/**
 * Object Pool - Manage Course Allocations
 * @author Prajakta Pawar
 */
public class ObjectPool {
	/**
	 * Object Pool Instance
	 */
	private static ObjectPool reusablePool = null;
	
	/**
	 * Concurrent Hashmap to hold course information
	 */
	private Map<Character, CoursesInfo> courseMap = new ConcurrentHashMap<Character, CoursesInfo>();	
	
	/**
	 * private constructors
	 */
	private ObjectPool(){
		Logger.writeMessage(
				"\n--------------------------- In ObjectPool Constructor ---------------------------\n"
				+"\t\t\n"
				+"------------------------------------------------------------------------\n"
				, Logger.DebugLevel.CONSTRUCTOR);
	}
	/**
	 * getInstance method 	which will return single instance of Object Pool
	 * @return instance of Object Pool
	 */
	public static ObjectPool getInstance() {
		if (reusablePool == null) {
			synchronized (ObjectPool.class) {
				if (reusablePool == null) {
					reusablePool = new ObjectPool();
				}
			}
		}
		return reusablePool;
	}
	/** 
	 *Comparator to sort seats available for each course.
	 */
	public static class PQsort implements Comparator<CoursesInfo> {
		@Override
		public int compare(CoursesInfo s1, CoursesInfo s2) {
			int seats1 = s1.getSeatsAvailable();
			int seats2 = s2.getSeatsAvailable();
			int rc = seats2 - seats1;
			return rc;
			}

	};
	
	/**
	 * object pool method to add objects into the course pool.
	 */
	public void addObjects(){
		char[] CourseArray = {'A','B','C','D','E','F','G','H'};
		for(int i=0;i<CourseArray.length;i++){
			CoursesInfo course = new CoursesInfo(CourseArray[i]);
			courseMap.put(course.getCourseID(),course);	
		}
	}
	/**
	 * Borrow object of type CoursesInfo from Object Pool.
	 */
	public CoursesInfo borrowObject(Character courseID, int nextIndex) {
		if (courseID == null) {
			List<CoursesInfo> queue= null;
			queue = new ArrayList<CoursesInfo>(courseMap.values());
			PQsort sort = new PQsort();
			Collections.sort(queue,sort);
			courseID = queue.get(nextIndex).getCourseID();
		}		
		CoursesInfo course = courseMap.remove(courseID);
		return course;
	}
	
	/**
	 * object pool return method to return object to Object Pool after its use.
	 */
	public void returnObject(CoursesInfo course) {
		if (course != null) {
			courseMap.put(course.getCourseID(), course);
		}
	}
	
	@Override
		public String toString() {
			// TODO Auto-generated method stub
			return "Courses Info :"+ Arrays.toString(courseMap.keySet().toArray());
		}
	
}
