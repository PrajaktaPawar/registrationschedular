package registrationScheduler.objectPool;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import registrationScheduler.util.Logger;

/**
 * Course Details
 * @author Prajakta Pawar
 *
 */
public class CoursesInfo {

	private Character courseID ;
	private int courseCapacity = 60;
	private int seatsAvailable = courseCapacity;
	
	public int getSeatsAvailable() {
		return seatsAvailable;
	}
	public void setSeatsAvailable(int seatsAvailable) {
		this.seatsAvailable = seatsAvailable;
	}

	private Map<Character,Integer> studentAllocatedToCourse;
	
	
	public Map<Character, Integer> getStudentAllocatedToCourse() {
		return studentAllocatedToCourse;
	}
	public void setStudentAllocatedToCourse(Map<Character, Integer> studentAllocatedToCourse) {
		this.studentAllocatedToCourse = studentAllocatedToCourse;
	}
	public char getCourseID() {
		return courseID;
	}
	public void setCourseID(char courseID) {
		this.courseID = courseID;
	}
	public int getCourseCapacity() {
		return courseCapacity;
	}
	public void setCourseCapacity(int courseCapacity) {
		this.courseCapacity = courseCapacity;
	}
	
	/**
	 * CoursesInfo Constructor
	 * @param id - student id
	 */
	public CoursesInfo(char id){
		courseID = id;
		setStudentAllocatedToCourse(new HashMap<Character, Integer>());
		Logger.writeMessage("In CoursesInfo constructor,course id initialized..\n"
				, Logger.DebugLevel.CONSTRUCTOR);
	}
	
	
	/**
	 * @return boolean - allocate seats if available and return true else false
	 */
	public boolean allocateIfAvailable(){
		if(0 < seatsAvailable){
			seatsAvailable--;
			return true;
		}else
			return false;
	}
	/**
	 * @return boolean - deallocate seats and return true else false
	 */
	public boolean dropIfNecessary(){
		if(courseCapacity > seatsAvailable){
			seatsAvailable++;
			return true;
		}else
			return false;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return " Course Id: "+courseID +
				" \n courseCapacity :"+ courseCapacity + 
				" \n seatsAvailable : "+ seatsAvailable + 
				" \n Assigned Courses :"+ Arrays.toString(studentAllocatedToCourse.keySet().toArray());
			
	}
}
