package registrationScheduler.threadMgmt;

import java.util.ArrayList;
import java.util.List;

import registrationScheduler.store.Results;
import registrationScheduler.util.Schedular;
import registrationScheduler.util.FileProcessor;
import registrationScheduler.util.Logger;
import registrationScheduler.util.StdoutDisplayInterface;

/**
 * This class is responsible for creating worker based on passed number of
 * threads.
 * 
 * @author Prajakta Pawar
 *
 */
public class CreateWorkers {
	private FileProcessor fileProcessor = null;
	private Schedular schedular = null;
	private Results results = null;

	/**
	 * CreateWorkers Constructor
	 * 
	 * @param fileProcessorIn
	 *            = FileProcessor class
	 * @param schedularIn
	 *            = Schedular class
	 * @param resultsStdoutDisplayIn
	 *            = Results class
	 */
	public CreateWorkers(FileProcessor fileProcessorIn, Schedular schedularIn,
			StdoutDisplayInterface resultsStdoutDisplayIn) {
		fileProcessor = fileProcessorIn;
		schedular = schedularIn;
		results = (Results) resultsStdoutDisplayIn;
		Logger.writeMessage("In CreateWorkers constructor", Logger.DebugLevel.CONSTRUCTOR);

	}

	/**
	 * Method to start worker threads.
	 * 
	 * @param num_threads
	 *            - Number of Threads
	 */
	public void startWorkers(int num_threads) {
		WorkerThread worker = new WorkerThread(fileProcessor, schedular, results);
		List<Thread> threads = new ArrayList<Thread>();

		for (int i = 1; i <= num_threads; i++) {
			Thread thread = new Thread(worker);
			threads.add(thread);
		}

		for (Thread thread : threads) {
			thread.start();
		}

		for (Thread thread : threads) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally{
				
				 /* try{ fileProcessor.closeScanner(); }catch(Exception e){
				  e.printStackTrace(); }finally{}
				 */
			}
		}

	}
}
