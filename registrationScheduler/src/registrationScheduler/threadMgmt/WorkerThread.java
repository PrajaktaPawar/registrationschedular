package registrationScheduler.threadMgmt;

import registrationScheduler.store.Results;
import registrationScheduler.util.Schedular;
import registrationScheduler.store.Student;
import registrationScheduler.util.FileProcessor;
import registrationScheduler.util.Logger;

/**
 * Worker Thread class which implements runnable 
 * @author Prajakta Pawar
 *
 */
public class WorkerThread implements Runnable {

	/**
	 * Results Object holds all results
	 */
	private Results results = null;
	
	/**
	 * FileProcessor object used to read line from file
	 */
	private FileProcessor fileProcessor = null;
	
	/**
	 * Schedular object -  used to do actual course assignment 
	 * and run allocation algorithm
	 */
	private Schedular schedular = null;
	
	/**
	 * Worker Constructor
	 * @param fileProcessorIn
	 * @param schedularIn
	 * @param resultsIn
	 */
	public WorkerThread(FileProcessor fileProcessorIn, Schedular schedularIn, Results resultsIn) {
		fileProcessor = fileProcessorIn;
		schedular = schedularIn;
		results = resultsIn;
		Logger.writeMessage("In Worker constructor..fileProcessor,schedular,results objects are initialized..", Logger.DebugLevel.CONSTRUCTOR);
	}

	/**
	 * Run method of Runnable interface
	 */
	@Override
	public void run() {
		// TODO Auto-generated method stub

		try {
		Logger.writeMessage("\n ********Start******** WorkerThread Run Method ********Start********", Logger.DebugLevel.THREAD_RUN);
        	String status = fileProcessor.getStatus();
			String line = fileProcessor.readString();
			while (line != null) {
				switch (status) {
				case "create":
					createStudentData(line);
					break;
				case "update":
					updateStudentData(line);
					break;
				}
				line = fileProcessor.readString();
			}
		Logger.writeMessage("\n ********End******** WorkerThread Run Method ********End********", Logger.DebugLevel.THREAD_RUN);
    		
		} catch (Exception e) {
			System.err.println(" Exception Occured..");
			e.printStackTrace();
			System.exit(0);
		} finally {
			/*
			 * try{ fileProcessor.closeScanner(); }catch(Exception e){
			 * e.printStackTrace(); }finally{}
			 */
		}
	}

	/**
	 * Method to call addStudentDataToMap and processStudent methods.
	 * @param line - line read from preferences input file.
	 */
	public void createStudentData(String line) {
		Student currentStudentID = null;
		currentStudentID = results.addStudentDataToMap(line);
		schedular.processStudent(currentStudentID);
	}

	/**
	 * Method to call updateStudent method by passing specific student
	 * @param line - line read from add drop input file.
	 */
	public void updateStudentData(String line) {
		String[] tabSeparatedArray = line.split("\\s");
		Student s = results.getStudent(tabSeparatedArray[0]);
		schedular.updateStudent(s, tabSeparatedArray);
	}

}
