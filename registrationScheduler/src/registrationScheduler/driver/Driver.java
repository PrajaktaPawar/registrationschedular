
package registrationScheduler.driver;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import registrationScheduler.threadMgmt.CreateWorkers;
import registrationScheduler.store.Results;
import registrationScheduler.util.Schedular;
import registrationScheduler.util.StdoutDisplayInterface;
import registrationScheduler.util.FileProcessor;
import registrationScheduler.util.Logger;

public class Driver {

	public static void main(String args[]) {
		
		FileProcessor fileProcessorIn = null;
		FileProcessor fileProcessorAddDrop = null;
		try {
			validateCommandLineArguments(args);
			int num_threads = Integer.parseInt(args[3]);
			int debug_level = Integer.parseInt(args[4]);
			Logger.setDebugValue(debug_level);
			try {
				 fileProcessorIn = new FileProcessor(new Scanner(new File(args[0])),"create");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				System.err.println("Registration preference input file not found..");
				e.printStackTrace();
			}
			
			try {
				 fileProcessorAddDrop = new FileProcessor(new Scanner(new File(args[1])),"update");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				System.err.println("Add Drop input file not found..");
				e.printStackTrace();
			}
			
			Schedular schedular = new Schedular();
			StdoutDisplayInterface results = new Results();
			CreateWorkers createWorkers = new CreateWorkers(fileProcessorIn,schedular,results);
			createWorkers.startWorkers(num_threads);
			
			createWorkers = new CreateWorkers(fileProcessorAddDrop,schedular,results);
			createWorkers.startWorkers(num_threads);
			
			results.writeSchedulesToScreen();
			((Results) results).writeSchedulesToFile(args[2]);
			
		} catch (Exception e) {
			System.err.println(" Exception Occured..");
			e.printStackTrace();
			System.exit(0);
		} finally {

		}

	} // end main(...)

	/**
	 * method to validate command line arguments
	 * 
	 * @param args
	 */
	private static void validateCommandLineArguments(String args[]) {
		// TODO Auto-generated method stub

		if (args.length != 5) {
			System.err.println("Some arguments are missing..");
			System.exit(0);
		}
		try {
			if((Integer.parseInt(args[4])) < 0 || (Integer.parseInt(args[4])) > 4){
				System.err.println("Debug Value should be between 0 and 4..");
				System.exit(0);
			}
		} catch (NumberFormatException e) {
			System.err.println("String Cannot be converted to integer..");
			e.printStackTrace();
			System.exit(0);
		}
		

		try {
			if ((Integer.parseInt(args[3])) < 1 || (Integer.parseInt(args[3])) > 3){
				System.err.println("Number of threads should be between 1 and 3..");
				System.exit(0);
			}
		} catch (NumberFormatException e) {
			System.err.println("String Cannot be converted to integer..");
			e.printStackTrace();
			System.exit(0);
		}

	}

} // end public class Driver
