Registration-Scheduler
A Multi-threaded application to assign courses to students based on their preferences.
Project Description:

There are 8 courses (A, B, C, D, E, F, G, H) being offered in the summer session. The capacity for each course is 60. The total number of students is 80. Each student is required to provide preference for 5 courses. The student is asked to provide a preference for each of the courses. Top preference is specified as "6", while the lowest preference is specified as "0". A student can occur in the input file multiple times. Each occurrence is either for requesting new courses for registration, or for course(s) that (s)he wishes to drop.

Output file will have average preference_score.
Sample of input files:

reg-preference.txt input file.

Student_1 A B E F C Student_2 C H G D A ...

Student_80 H F A C B

add-drop.txt input file (The digit after the student name is for add/drop: 0 indicates drop, 1 indicates add).

Student_2 0 H A Student_2 1 B E

Student_1 0 E

Debud levels used:

DEBUG_VALUE=4 Print to stdout everytime a constructor is called DEBUG_VALUE=3 Print to stdout everytime a thread's run() method is called DEBUG_VALUE=2 Print to stdout everytime an entry is added to the Results data structure DEBUG_VALUE=1 Print to stdout the contents of the data structure in the store DEBUG_VALUE=0 Prints Average preference code

Steps to execute code:
Assuming you are in the directory containing this README:

## To clean:
ant -buildfile src/build.xml -Darg0=reg-preference.txt -Darg1=add-drop.txt -Darg2=output.txt -Darg3=1 -Darg4=0 clean

-----------------------------------------------------------------------
## To compile: 
ant -buildfile src/build.xml -Darg0=reg-preference.txt -Darg1=add-drop.txt -Darg2=output.txt -Darg3=1 -Darg4=0

-----------------------------------------------------------------------
## To run by specifying arguments from command line 
## We will use this to run your code
ant -buildfile src/build.xml run -Darg0=reg-preference.txt -Darg1=add-drop.txt -Darg2=output.txt -Darg3=1 -Darg4=0

-----------------------------------------------------------------------

## To create tarball for submission
tar -cvf prajakta_pawar_assign_2.tar prajakta_pawar_assign_2/
gzip prajakta_pawar_assign_2.tar

## To untar the tarball:
tar -zxvf prajakta_pawar_assign_2.tar.gz



-----------------------------------------------------------------------

"I have done this assignment completely on my own. I have not copied
it, nor have I given my solution to anyone else. I understand that if
I am involved in plagiarism or cheating I will have to sign an
official form that I have cheated and that this form will be stored in
my official university record. I also understand that I will receive a
grade of 0 for the involved assignment for my first offense and that I
will receive a grade of F for the course for any additional
offense.”

[Date: ] -- 03/07/2017

-----------------------------------------------------------------------

Provide justification for Data Structures used in this assignment in
term of Big O complexity (time and/or space)

HashMap - O(1) - To store student data from registration preferences input file. Hashmap works on principle of hashing and 
internally uses hashcode as a base, for storing key-value pair.With the help of hashcode, Hashmap distribute 
the objects across the buckets in such a way that hashmap put the objects and retrieve it in constant time O(1).


Concurrent Hashmap - To store course data.Thread safe without synchronizing the whole map so no performance 
overhead.ConcurrentHashMap doesn’t throw a ConcurrentModificationException if one thread tries to modify it 
while another is iterating over it.There is no locking at the object level.Requires constant time i.e. O(1).
-----------------------------------------------------------------------

Provide list of citations (urls, etc.) from where you have taken code
(if any).

